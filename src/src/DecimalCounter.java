package src;

public class DecimalCounter {
    public final int from;
    public final int to;

    private int counter;

    public DecimalCounter(int from, int to, int startValue) {
        this.from = from;
        this.to = to;

        this.counter = startValue;
    }

    public DecimalCounter() {
        this(0, 10, 0);
    }

    public void increase() {
        if (counter == to) {
            return;
        }

        counter++;
    }

    public void decrease() {
        if (counter == from) {
            return;
        }

        counter--;
    }

    public int value() {
        return counter;
    }
}
