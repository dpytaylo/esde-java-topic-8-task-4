package src;

public class Main {
    public static void main(String[] args) {
        var counter = new DecimalCounter();

        System.out.println("Increase:");
        for (int i = 0; i < 11; i++) {
            counter.increase();
            System.out.println(i + 1 + ". " + counter.value());
        }

        var counter2 = new DecimalCounter(0, 4, 3);

        System.out.println("Decrease:");
        for (int i = 0; i < 4; i++) {
            counter2.decrease();
            System.out.println(i + 1 + ". " + counter2.value());
        }
    }
}
